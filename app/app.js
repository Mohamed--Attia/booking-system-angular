(function(){'use strict';angular.module("myApp",[]).controller("ReservationController",ReservationController);ReservationController.$inject=['$scope'];function ReservationController($scope){var vm=this;vm.noOfadults=0;vm.noOfChildren=0;vm.selectedNumOfRoom=1;vm.selectedAdultsData=[];vm.selectedChildrenData=[];vm.noOfSelectedChilds=[]
ShowData(1,1,0);vm.OnSelectedRoom=function(){vm.rooms=[];for(var i=0;i<vm.selectedNumOfRoom;i++){vm.rooms.push({index:i})}
ShowData(vm.selectedNumOfRoom,vm.noOfadults,vm.noOfChildren)}
vm.OnSelectedAdults=function(index){var target_room=vm.selectedAdultsData.find(e=>e.index==index);if(target_room){if(vm.selectedAdult[index].value>target_room.value){vm.noOfadults+=vm.selectedAdult[index].value-target_room.value;vm.selectedAdultsData[index].value=vm.selectedAdult[index].value}
else{vm.noOfadults-=target_room.value-vm.selectedAdult[index].value;vm.selectedAdultsData[index].value=vm.selectedAdult[index].value}}
else{vm.selectedAdultsData.push({index:index,value:vm.selectedAdult[index].value});vm.noOfadults+=vm.selectedAdult[index].value}
ShowData(vm.selectedNumOfRoom,vm.noOfadults,vm.noOfChildren)}
vm.OnSelectedChildren=function(index,roomIndex){var target_room=vm.selectedChildrenData.find(e=>e.index==index);if(target_room){if(vm.selectedChildren[index].value>target_room.value){vm.noOfChildren+=vm.selectedChildren[index].value-target_room.value;vm.selectedChildrenData[index].value=vm.selectedChildren[index].value}
else{vm.noOfChildren-=target_room.value-vm.selectedChildren[index].value;vm.selectedChildrenData[index].value=vm.selectedChildren[index].value}}
else{vm.selectedChildrenData.push({index:index,value:vm.selectedChildren[index].value});vm.noOfChildren+=vm.selectedChildren[index].value}
if(vm.selectedChildren[index].value>0){var tempArr=[];for(var i=0;i<vm.selectedChildren[index].value;i++){tempArr.push(i)}
if(!target_room){vm.noOfSelectedChilds.push({childAges:tempArr,roomNo:roomIndex})}
else{if(vm.noOfSelectedChilds[index]||vm.noOfSelectedChilds.length==0){if(vm.noOfSelectedChilds.length==0){vm.noOfSelectedChilds.push({childAges:tempArr,roomNo:roomIndex})}
else{vm.noOfSelectedChilds[index].childAges=tempArr}}}}
else{if(vm.noOfSelectedChilds[index]){vm.noOfSelectedChilds[index].childAges=[]}}
ShowData(vm.selectedNumOfRoom,vm.noOfadults,vm.noOfChildren)}
function ShowData(rooms,adults,children){vm.showDataDetails="Rooms "+rooms+' '+" Adults "+adults+" Children "+children}}}())